package com.demo1.example.model;


import lombok.AllArgsConstructor;
import lombok.Data;
 
@Data @AllArgsConstructor 
public class Products {

	private Long id;
	
	private String name;
}
