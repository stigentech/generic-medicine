package com.demo1.example.controller;


import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo1.example.model.Products;
import com.demo1.example.dao.ProductDao;

@RestController
public class ProductsController {
	
	@Autowired
	ProductDao productdao;
	
	@GetMapping("/products")
	public List<Products> getProducts(@RequestParam String query){
		
		System.out.println("query: "+query);
		
		List<Products> list = productdao.findAll(query);
		System.out.println("Display all customers...");
       
        list.forEach(x -> System.out.println(x));

        System.out.println("Done!");
      
		 
		return list;
		
	}

}
