package com.demo1.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.demo1.example.dao.ProductDao;
import com.demo1.example.model.Products;
import static java.lang.System.exit;

@SpringBootApplication
public class ExampleApplication  {
	
	@Autowired
	ProductDao productdao;

	public static void main(String[] args) {
		SpringApplication.run(ExampleApplication.class, args);
	}
	

}
