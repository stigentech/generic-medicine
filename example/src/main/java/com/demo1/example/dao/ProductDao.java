package com.demo1.example.dao;

import org.springframework.stereotype.Repository;

import com.demo1.example.model.Products;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;



@Repository
public class ProductDao {
	
	 @Autowired
	    private JdbcTemplate jdbcTemplate;

		// thanks Java 8, look the custom RowMapper
	    public List<Products> findAll(String q) {
	    	

	    	List<Products> result = jdbcTemplate.query(
	                "SELECT * FROM test where name like '%"+q+"%'",
	                (rs, rowNum) -> new Products(rs.getInt(1),
	                        rs.getString(2))
	        );
	    	
	    

	        return result;

	    }


}
